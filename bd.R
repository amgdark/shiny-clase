# install.packages('RMariaDB')
library(RMariaDB)


conexion <- dbConnect(
  RMariaDB::MariaDB(),
  dbname='autenticacion',
  host='localhost',
  port=3306,
  user='shiny', # root
  password='shiny1234' # password de instalación de MariaDB
)

loginBD <- function(username, password){
  resultado = dbSendQuery(
    conexion,
    "select * from usuario where username = ? and password = sha2(?, 256)",
    params = list(username, password)
  )
  datos = dbFetch(resultado, n = -1)
  
  return(datos)
}

insertarUsuario <- function(usuario){
  dbSendStatement(
    conexion,
    'insert into usuario (username, password, email, id_grupo) values (?,sha2(?, 256),?,?)',
    params = list(usuario$username, usuario$password, usuario$email, usuario$grupo)
  )
  
  return(TRUE)
}

obtieneUsuarioBD <- function(){
  resultado = dbSendQuery(
    conexion,
    "select username, email, id_grupo from usuario"
  )
  datos = dbFetch(resultado, n = -1)
  
  return(datos)
}